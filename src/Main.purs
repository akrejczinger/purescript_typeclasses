module Main where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, log)
import Data.Array
import Data.Foldable
import Data.Maybe
import Data.Monoid
import Partial.Unsafe

newtype Complex = Complex
  { real :: Number
  , imaginary :: Number
  }

instance showComplex :: Show Complex where
  show (Complex {real, imaginary}) =
    "Complex {real: " <> show real <> ", imaginary: " <> show imaginary <> "}"

instance eqComplex :: Eq Complex where
  eq (Complex a) (Complex b) =
    a.real == b.real && a.imaginary == b.imaginary

data NonEmpty a = NonEmpty a (Array a)

instance showNonEmpty :: Show a => Show (NonEmpty a) where
  show (NonEmpty a arr) = "NonEmpty " <> show a <> " " <> show arr

instance eqNonEmpty :: (Eq a) => Eq (NonEmpty a) where
  eq (NonEmpty init1 arr1) (NonEmpty init2 arr2) = init1 == init2 && arr1 == arr2

instance semigroupNonEmpty :: Semigroup (NonEmpty a) where
  append (NonEmpty init1 arr1) (NonEmpty init2 arr2) =
    NonEmpty init1 (arr1 <> [init2] <> arr2)

instance functorNonEmpty :: Functor NonEmpty where
  map func (NonEmpty init arr) = NonEmpty (func init) (map func arr)

instance foldableNonEmpty :: Foldable NonEmpty where
  foldl func acc (NonEmpty first rest) =
    foldl func newAcc rest where
      newAcc = func acc first

  foldr func acc (NonEmpty first rest) =
    foldr func newAcc rest where
      newAcc = func first acc

  foldMap func (NonEmpty first rest) =
    (func first) <> (foldMap func rest)

data Extended a = Finite a | Infinite

instance eqExtended :: (Eq a) => Eq (Extended a) where
  eq Infinite Infinite = true
  eq (Finite a) (Finite b) = a == b
  eq _ _ = false

instance ordExtended :: (Ord a) => Ord (Extended a) where
  compare Infinite Infinite = EQ
  compare Infinite (Finite _) = GT
  compare (Finite _) Infinite = LT
  compare (Finite a) (Finite b) | a < b = LT
  compare (Finite a) (Finite b) | a > b = GT
  compare _ _ = EQ

data OneMore f a = OneMore a (f a)

instance foldableOneMore :: Foldable f => Foldable (OneMore f) where
  foldl func acc (OneMore first rest) =
    foldl func newAcc rest where
      newAcc = func acc first

  foldr func acc (OneMore first rest) =
    foldr func newAcc rest where
      newAcc = func first acc

  foldMap func (OneMore first rest) =
    (func first) <> (foldMap func rest)

unsafeMax :: Partial => Array Int -> Int
unsafeMax arr = fromJust $ maximum arr

newtype Multiply = Multiply Int

instance semigroupMultiply :: Semigroup Multiply where
  append (Multiply n) (Multiply m) = Multiply (n * m)

instance monoidMultiply :: Monoid Multiply where
  mempty = Multiply 1

instance showMultiply :: Show Multiply where
  show (Multiply n) = "Multiply " <> (show n)

class Monoid m <= Action m a where
  act :: m -> a -> a

instance repeatAction :: Action Multiply String where
  act (Multiply n) string = foldl append "" (replicate n string)

main :: forall e. Eff (console :: CONSOLE | e) Unit
main = log $ show $ act (Multiply 4) "empty"
